<?php   
    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    require_once("./funciones/calcular_subtotal.php");
    require_once("./funciones/obtener_estatus.php");
    require_once("./funciones/fecha_actual.php");

    $tipo = "remision";
    $fecha = "";
    $lugar = "";
    $contador = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    //Consulta para verificar sí la remisión ya se inserto
    $consultaVerificaRemision = "SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=? LIMIT 1";
    $resultadoVerificaRemision = $baseGodaddy->prepare($consultaVerificaRemision);
    //Consulta para insertar la información en la nueva tabla "REPORTE_DE_VENTAS"
    $consultaInsertarReporte = "INSERT INTO REPORTE_DE_VENTAS VALUES(?,?,?,?,?,?,
                                    (SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=? LIMIT 1),
                                    (SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=? LIMIT 1))";
    $resultadoInsertarReporte = $baseGodaddy->prepare($consultaInsertarReporte);
    //Consulta para insertar las nuevas remisiones
    $consultaInsertaRemisiones = "INSERT INTO CARGAS(idFacturaRemision, CLAVE, CLIENTE, NOMBRE, ESTATUS, FECHA, 
                                    DESCUENTO, IMPORTE, VENDEDOR, Entrada_Contado)
                                    VALUES(?,?,?,?,?,?,?,?,?,?)";
    $resultadoInsertarRemisiones = $baseGodaddy->prepare($consultaInsertaRemisiones);
    //Consulta para obtener las partidas por remision
    $consultaPartidasRemision = "SELECT CVE_ART, CANT, DESC1, TOT_PARTIDA FROM PAR_FACTR01 WHERE CVE_DOC=?";
    $resultadoPartidasRemision = $baseSAE->prepare($consultaPartidasRemision);
    //Consulta para obtener las remisiones posteriores a una fecha dada
    $consultaRemisiones = "SELECT CVE_DOC, CVE_CLPV, FECHA_DOC, FACTR01.STATUS, DES_TOT_PORC, IMPORTE,
                            CLIE01.NOMBRE AS CLIENTE, VEND01.NOMBRE AS VENDEDOR, CLIE01.NOMBRE FROM FACTR01
                            INNER JOIN VEND01 ON FACTR01.CVE_VEND=VEND01.CVE_VEND
                            INNER JOIN CLIE01 ON FACTR01.CVE_CLPV=CLIE01.CLAVE
                            WHERE FECHA_DOC>? AND FECHA_DOC<?";
    $resultadoRemisiones = $baseSAE->prepare($consultaRemisiones);
    //Consulta para obtener la última fecha de remision
    $consultaFecha = "SELECT FECHA FROM CARGAS WHERE CLAVE LIKE ? ORDER BY FECHA DESC LIMIT 1";
    $resultadoFecha = $baseGodaddy->prepare($consultaFecha);
    $resultadoFecha->execute(array('%R%'));
    $registroFecha = $resultadoFecha->fetch(PDO::FETCH_ASSOC);
    $resultadoFecha->closeCursor();
    $fecha = $registroFecha["FECHA"];

    //Obteniendo las remisiones
    $resultadoRemisiones->execute(array($fecha, fecha_actual()));
    while($registroRemisiones = $resultadoRemisiones->fetch(PDO::FETCH_ASSOC)){
        //Verificando sí la remisión existe para poder insertarla
        $resultadoVerificaRemision->execute(array($registroRemisiones["CVE_DOC"]));
        if($resultadoVerificaRemision->rowCount()==0){
            //Insertar nuevas remisiones
            $resultadoInsertarRemisiones->execute(array(NULL, $registroRemisiones["CVE_DOC"], $registroRemisiones["CVE_CLPV"], 
                                                        $registroRemisiones["CLIENTE"], obtenerEstatus($registroRemisiones["STATUS"]), 
                                                        $registroRemisiones["FECHA_DOC"], $registroRemisiones["DES_TOT_PORC"], 
                                                        $registroRemisiones["IMPORTE"], $registroRemisiones["VENDEDOR"], 0));
            if($resultadoInsertarRemisiones->rowCount()==1){
                $contador++;
                switch($registroRemisiones["CVE_DOC"][1]){
                    case 'D':
                        $lugar = "Cedis Pozo Brasil";
                        break;

                    case 'T':
                        $lugar = "Cedis Tecámac";
                        break;

                    default:
                        $lugar = "Cedis Pozo Brasil";
                        break;
                }
                $resultadoPartidasRemision->execute(array($registroRemisiones["CVE_DOC"]));
                while($registroPartidas = $resultadoPartidasRemision->fetch(PDO::FETCH_ASSOC)){
                    //Calcular el subtotal de cada partida
                    $subtotal = subtotal($registroPartidas["DESC1"], $registroPartidas["TOT_PARTIDA"]);
                    //Insertar la información dentro de la tabla "REPORTE_DE_VENTAS"
                    $resultadoInsertarReporte->execute(array(NULL, $subtotal, $registroPartidas["CANT"], 
                                                        obtenerEstatus($registroRemisiones["STATUS"]), $tipo, $lugar, 
                                                        $registroRemisiones["CVE_DOC"], $registroPartidas["CVE_ART"]));
                }
            }
        }
    }
    $resultadoVerificaRemision->closeCursor();
    $resultadoInsertarReporte->closeCursor();
    $resultadoInsertarRemisiones->closeCursor();
    $resultadoPartidasRemision->closeCursor();
    $resultadoRemisiones->closeCursor();
    
    $baseGodaddy = null;
    $baseSAE = null;
    
    echo "Se insertaron un total de " . $contador . " remisiones con sus respectivas partidas";
?>