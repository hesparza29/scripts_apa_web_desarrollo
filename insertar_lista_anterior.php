<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");

    $contador = 0;
    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    
    //Consulta para insertar los productos
    $consultaInsertarProducto = "INSERT INTO PRODUCTOS VALUES(?,?,?,?,?,?,?,?,?)";
    $resultadoInsertarProducto = $baseGodaddy->prepare($consultaInsertarProducto);
    //Consulta para obtener los precios por lista
    $consultaPrecios = "SELECT INVE01.CVE_ART, INVE01.DESCR, PRECIO_X_PROD01.PRECIO, 
                            INVE01.LIN_PROD, INVE_CLIB01.CAMPLIB8
                            FROM INVE01 INNER JOIN
                            INVE_CLIB01 ON
                            INVE01.cve_art=INVE_CLIB01.cve_prod 
                            INNER JOIN PRECIO_X_PROD01 ON PRECIO_X_PROD01.CVE_ART=INVE01.CVE_ART 
                            WHERE (LIN_PROD=? OR LIN_PROD=? OR LIN_PROD=?)
                            AND STATUS=? AND PRECIO_X_PROD01.CVE_PRECIO=?";
    $resultadoPrecios = $baseSAE->prepare($consultaPrecios);
    $resultadoPrecios->execute(array('1', '2', '3', 'A', '1'));
    while($registroPrecios = $resultadoPrecios->fetch(PDO::FETCH_ASSOC)){
        //echo $registroPrecios["CVE_ART"] . " " . $registroPrecios["DESCR"] . " " . $registroPrecios["CAMPLIB8"] . "<br />";
        $resultadoInsertarProducto->execute(array($registroPrecios["CVE_ART"], $registroPrecios["DESCR"], 
                                                    $registroPrecios["PRECIO"], $registroPrecios["CAMPLIB8"], 
                                                    $registroPrecios["LIN_PROD"], '', 0.00, 0.00, 'C'));
        if($resultadoInsertarProducto->rowCount()==1){
            $contador++;
        }
    }
    $resultadoPrecios->closeCursor();
    $resultadoInsertarProducto->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se insertaron un total de "  . $contador . " productos";
?>