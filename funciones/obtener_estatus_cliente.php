<?php

  //Función que me permite obtener el estatus del Cliente
  function obtenerEstatusCliente($letraEstatus){
    $estatus = "";

    switch ($letraEstatus){
      case 'A':
        $estatus = "Activo";
      break;

      case 'B':
        $estatus = "Baja";
      break;

      case 'M':
        $estatus = "Moroso";
      break;

      case 'S':
        $estatus = "Suspendido";
      break;

      default:
        $estatus = "Error";
      break;
    }

    return $estatus;
  }

?>