<?php

    //Función que calcula subtotal de cada producto, recibe descuento e importe
    function subtotal($descuento, $importe){
        if($descuento!=0){
            $descuento = $importe*($descuento/100);
            $importe = round(($importe-$descuento)*100)/100;
            return $importe;
        }
        else{
            return $importe;
        }
    }
?>