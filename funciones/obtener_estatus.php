<?php

  //Función que me permite obtener el estatus de una Factura o Remisión en
  //función a la letra que devuelva la  consuta a la base de datos
  function obtenerEstatus($letraEstatus){
    $estatus = "";

    switch ($letraEstatus){
      case 'E':
        $estatus = "Emitida";
      break;

      case 'O':
        $estatus = "Original";
      break;

      case 'C':
        $estatus = "Cancelada";
      break;
      default:
        $estatus = "Error";
      break;
    }

    return $estatus;
  }

?>