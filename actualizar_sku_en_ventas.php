<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");


    $baseGodaddy = conexionBBDD_Godaddy();
    $contador = 0;
    $nombreArchivo = fopen("../archivos_de_carga/cambio sku en compras.csv", "r") or die("Problemas al abrir el archivo");


    //Consulta para actualizar el sku dentro del reporte de ventas
    $consultaActualizarSku = "UPDATE REPORTE_DE_VENTAS SET 
                                idProducto=(SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?) 
                                WHERE idProducto=(SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?)";
    $resultadoActualizarSku = $baseGodaddy->prepare($consultaActualizarSku);
    while (!feof($nombreArchivo)){  
        $linea = fgets($nombreArchivo);
        $linea = trim($linea);

        //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
        if(strlen($linea)>0){
          $linea = explode(",", $linea);
          echo $linea[0] . " " . $linea[1] . "<br />";
          //Actualiza el sku dentro del reporte de venta
          $resultadoActualizarSku->execute(array($linea[1], $linea[0]));
          if($resultadoActualizarSku->rowCount()==1){
              $contador++;
          }
        }

    }
    $resultadoActualizarSku->closeCursor();
    fclose($nombreArchivo);
    $baseGodaddy = null;

    echo "Se actualizaron un total de "  . $contador . " productos dentro del reporte de ventas";
?>