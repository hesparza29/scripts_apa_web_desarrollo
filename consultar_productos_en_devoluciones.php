<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $contenido = "Numero APA,Cantidad,Devolucion\n";
    $contador = 0;

    //Consulta para obtener la cantidad de devolucion por producto
    $consultaCantidades = "SELECT SUM(UNIDADESxSKU) AS TOTAL FROM NOTAS WHERE SKU=? AND DEVOLUCION=?";
    $resultadoCantidades = $baseGodaddy->prepare($consultaCantidades);
    //Consulta para obtener los productos que se han devuelto
    $consultaProductos = "SELECT DISTINCT SKU FROM NOTAS";
    $resultadoProductos = $baseGodaddy->prepare($consultaProductos);
    $resultadoProductos->execute(array());
    while($registroProductos = $resultadoProductos->fetch(PDO::FETCH_ASSOC)){
        //Obtener cantidad de devoluciones del producto(Almacen)
        $contenido .= $registroProductos["SKU"] . ",";
        $resultadoCantidades->execute(array($registroProductos["SKU"], 'Almacen'));
        $registroCantidades = $resultadoCantidades->fetch(PDO::FETCH_ASSOC);
        $contenido .= $registroCantidades["TOTAL"] . ",";
        $contenido .= "Almacen\n";
        //Obtener cantidad de devoluciones del producto(Merma)
        $contenido .= $registroProductos["SKU"] . ",";
        $resultadoCantidades->execute(array($registroProductos["SKU"], 'Merma'));
        $registroCantidades = $resultadoCantidades->fetch(PDO::FETCH_ASSOC);
        $contenido .= $registroCantidades["TOTAL"] . ",";
        $contenido .= "Merma\n";
        $contador++;
    }
    $resultadoCantidades->closeCursor();
    $resultadoProductos->closeCursor();


    $baseGodaddy = null;

    //Creando el archivo
    $archivo = fopen("../archivos_de_descarga/devoluciones por producto.csv", "w");
    fwrite($archivo, $contenido);
    fclose($archivo);

    echo "Hay un total de " . $contador . " productos<br />";
?>