<?php   
    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    require_once("./funciones/obtener_estatus_cliente.php");

    $estatusActivo = "A";
    $estatusSuspendido = "S";
    $clave = "MOSTR";
    $contador = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();

    //Consulta para actualizar la información de los clientes
    $consultaActulizaClientes = "UPDATE CLIENTE SET NOMBRE=?, idDescuento=(SELECT idDescuento FROM DESCUENTO WHERE Porcentaje=?), 
                                    RFC=?, Calle=?, Colonia=?, Telefono=?, 
                                    Estatus=?, idVendedor=(SELECT idVendedor FROM VENDEDOR WHERE idVendedor=?) WHERE idCliente=?";
    $resultadoActualizaClientes = $baseGodaddy->prepare($consultaActulizaClientes);
    //Consulta para obtener los clientes activos
    $consultaClientes = "SELECT CLAVE, NOMBRE, DESCUENTO, RFC, CALLE, COLONIA,
                            CODIGO, TELEFONO, STATUS, CVE_VEND FROM CLIE01 
                            WHERE (STATUS=? OR STATUS=?) AND CLAVE!=? 
                            ORDER BY CLAVE ASC";
    $resultadoClientes = $baseSAE->prepare($consultaClientes);
    $resultadoClientes->execute(array($estatusActivo, $estatusSuspendido, $clave));
    while($registroClientes = $resultadoClientes->fetch(PDO::FETCH_ASSOC)){
        $resultadoActualizaClientes->execute(array($registroClientes["NOMBRE"], (empty($registroClientes["DESCUENTO"]) ? 1 : $registroClientes["DESCUENTO"]), 
                                                $registroClientes["RFC"], $registroClientes["CALLE"], $registroClientes["COLONIA"], 
                                                $registroClientes["TELEFONO"], obtenerEstatusCliente($registroClientes["STATUS"]), 
                                                (empty($registroClientes["CVE_VEND"]) ? 1 : $registroClientes["CVE_VEND"]), $registroClientes["CLAVE"]));
        if($resultadoActualizaClientes->rowCount()==1){
            $contador++;
        }
        
    }
    
    $resultadoClientes->closeCursor();
    
    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizo la información de " . $contador . " clientes";
?>