<?php

    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    
    $estatus = "C";
    $estatusFinal = "Cancelada";
    $fechaInicio = "";
    $fechaFin = "";
    $contador = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();

    //Consulta para actualizar el estatus de las facturas
    $consultaActualizaFacturas = "UPDATE CARGAS SET ESTATUS=? WHERE CLAVE=?";
    $resultadoActualizaFacturas = $baseGodaddy->prepare($consultaActualizaFacturas);
    //Consulta para actualizar el estatus del reporte de ventas
    $consultaActualizaReporte = "UPDATE REPORTE_DE_VENTAS SET Estatus=? 
                                    WHERE idFacturaRemision=(SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=?)";
    $resultadoActualizaReporte = $baseGodaddy->prepare($consultaActualizaReporte);
    //Consulta para obtener la fecha de inicio
    $consultaFechaInicial = "SELECT DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AS FECHA FROM CARGAS LIMIT 1";
    $resultadoFechaInicial = $baseGodaddy->prepare($consultaFechaInicial);
    $resultadoFechaInicial->execute(array());
    $registroFechaInicial = $resultadoFechaInicial->fetch(PDO::FETCH_ASSOC);
    $fechaInicio = $registroFechaInicial["FECHA"];
    $resultadoFechaInicial->closeCursor();

    //Consulta para obtener la fecha final
    $consultaFechaFinal = "SELECT FECHA FROM CARGAS ORDER BY FECHA DESC LIMIT 1";
    $resultadoFechaFinal = $baseGodaddy->prepare($consultaFechaFinal);
    $resultadoFechaFinal->execute(array());
    $registroFechaFinal = $resultadoFechaFinal->fetch(PDO::FETCH_ASSOC);
    $fechaFin = $registroFechaFinal["FECHA"];
    $resultadoFechaFinal->closeCursor();

    //Consulta para obtener todas las facturas canceladas en un rango de fecha
    $consultaFacturas = "SELECT CVE_DOC FROM FACTF01 WHERE FECHA_DOC BETWEEN ? AND ? AND STATUS=?";
    $resultadoFacturas = $baseSAE->prepare($consultaFacturas);
    $resultadoFacturas->execute(array($fechaInicio, $fechaFin, $estatus));
    while($registroFacturas = $resultadoFacturas->fetch(PDO::FETCH_ASSOC)){
        //Actualizando las facturas
        $resultadoActualizaFacturas->execute(array($estatusFinal, $registroFacturas["CVE_DOC"]));
        //Actualizando las facturas del reporte de ventas
        $resultadoActualizaReporte->execute(array($estatusFinal, $registroFacturas["CVE_DOC"]));
        //Contabilizando cuantas facturas se actualizaron
        if($resultadoActualizaFacturas->rowCount()==1){
            $contador++;
        }
    }
    $resultadoActualizaFacturas->closeCursor();
    $resultadoActualizaReporte->closeCursor();
    $resultadoFacturas->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizaron un total de " . $contador . " facturas.";
?>