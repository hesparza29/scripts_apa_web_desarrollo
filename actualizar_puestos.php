<?php

    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $baseGodaddy = conexionBBDD_Local();
    $nombreArchivo = fopen("../archivos_de_carga/puestos.csv", "r") or die("Problemas al abrir el archivo");
    $contador = 0;

    //Consulta para actualizar información de los puestos
    $consultaActualizarPuesto = "UPDATE PUESTO SET Nombre=?, SalarioTipoA=?, SalarioTipoB=?, SalarioTipoC=?, 
                                    SalarioTipoD=?, Vacante=? WHERE idPuesto=?";
    $resultadoActualizarPuesto = $baseGodaddy->prepare($consultaActualizarPuesto);
    while (!feof($nombreArchivo)){
        $linea = fgets($nombreArchivo);
        $linea = trim($linea);

        //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
        if(strlen($linea)>0){
            $linea = explode(",", $linea);
            if($linea[0]!="idPuesto"){
                $resultadoActualizarPuesto->execute(array($linea[1], $linea[2], $linea[3], $linea[4], $linea[5], $linea[6], $linea[0]));
                $contador++;
            }
        }
    }

    fclose($nombreArchivo);

    $baseGodaddy = null;

    echo "Se actualizaron un total de " . $contador . " puestos dentro del organigrama.";
?>