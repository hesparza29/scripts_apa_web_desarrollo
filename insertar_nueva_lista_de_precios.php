<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $listaDePrecio = "Precio público";
    $idListaDePrecio = 0;
    $contador = 0;
    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    
    //Consulta para obtener el id de la lista de Precio
    $consultaLista = "SELECT idListaDePrecio FROM LISTA_DE_PRECIO WHERE Descripcion=?";
    $resultadoLista = $baseGodaddy->prepare($consultaLista);
    $resultadoLista->execute(array($listaDePrecio));
    $registroLista = $resultadoLista->fetch(PDO::FETCH_ASSOC);
    $resultadoLista->closeCursor();
    $idListaDePrecio = $registroLista["idListaDePrecio"];
    //Consulta para insertar los productos con la lista de precios seleccionada
    $consultaInsertaProducto = "INSERT INTO PRODUCTO_LISTA_DE_PRECIO VALUES(?,?,
                                (SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?),
                                (SELECT idListaDePrecio FROM LISTA_DE_PRECIO WHERE Descripcion=?))";
    $resultadoInsertaProducto = $baseGodaddy->prepare($consultaInsertaProducto);
    //Consulta para obtener la informacion de los productos
    $consultaInformacionProducto = "SELECT INVE01.CVE_ART, PRECIO_X_PROD01.PRECIO
                                    FROM INVE01 INNER JOIN PRECIO_X_PROD01 ON
                                    PRECIO_X_PROD01.CVE_ART=INVE01.CVE_ART
                                    WHERE (LIN_PROD=? OR LIN_PROD=? OR LIN_PROD=?)
                                    AND STATUS=? AND PRECIO_X_PROD01.CVE_PRECIO=? AND INVE01.STATUS=?";
    $resultadoInformacionProducto = $baseSAE->prepare($consultaInformacionProducto);
    $resultadoInformacionProducto->execute(array('1', '2', '3', 'A', '1', 'A'));
    while($registroInformacionProducto = $resultadoInformacionProducto->fetch(PDO::FETCH_ASSOC)){
        $resultadoInsertaProducto->execute(array(NULL, $registroInformacionProducto["PRECIO"], 
                                                    $registroInformacionProducto["CVE_ART"], 
                                                    $listaDePrecio));
        if($resultadoInsertaProducto->rowCount()==1){
            $contador++;
        }
    }
    $resultadoInsertaProducto->closeCursor();
    $resultadoInformacionProducto->closeCursor();

    $baseSAE = null;
    $baseGodaddy = null;

    echo "Se insertaron " . $contador . " productos de manera correcta";
?>