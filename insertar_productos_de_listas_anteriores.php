<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");

    $contador = 0;
    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    
    //Consulta para insertar los productos con todas sus listas de precios
    $consultaInsertarProducto = "INSERT INTO PRODUCTO_LISTA_DE_PRECIO VALUES(?,?,?,
                                    (SELECT idListaDePrecio FROM LISTA_DE_PRECIO WHERE idListaSAE=?))";
    $resultadoInsertarProducto = $baseGodaddy->prepare($consultaInsertarProducto);
    //Consulta para obtener las listas de precio
    $consultaLista = "SELECT idListaDePrecio, idListaSAE FROM LISTA_DE_PRECIO";
    $resultadoLista = $baseGodaddy->prepare($consultaLista);
    //Consulta para obtener los precios por lista
    $consultaPrecios = "SELECT PRECIO_X_PROD01.CVE_ART,PRECIOS01.CVE_PRECIO,PRECIO_X_PROD01.PRECIO FROM INVE01 
                        INNER JOIN INVE_CLIB01 ON INVE01.cve_art=INVE_CLIB01.cve_prod
                        INNER JOIN PRECIO_X_PROD01 ON PRECIO_X_PROD01.CVE_ART=INVE01.CVE_ART
                        INNER JOIN PRECIOS01 ON PRECIO_X_PROD01.CVE_PRECIO=PRECIOS01.CVE_PRECIO
                        WHERE (LIN_PROD=? OR LIN_PROD=? OR LIN_PROD=?) AND INVE01.STATUS=? 
                        AND INVE01.CVE_ART=? AND PRECIOS01.CVE_PRECIO=?";
    $resultadoPrecios = $baseSAE->prepare($consultaPrecios);
    //Consulta para obtener los productos que no se encuentran con sus listas de precio
    $consultaProductos = "SELECT idProducto, NumeroAPA FROM PRODUCTO WHERE idProducto 
                            NOT in (SELECT DISTINCT idProducto FROM PRODUCTO_LISTA_DE_PRECIO)";
    $resultadoProductos = $baseGodaddy->prepare($consultaProductos);
    $resultadoProductos->execute(array());
    while($registroProductos = $resultadoProductos->fetch(PDO::FETCH_ASSOC)){
        //Listas de precio
        $resultadoLista->execute(array());
        while($registroLista = $resultadoLista->fetch(PDO::FETCH_ASSOC)){
            //Obtener precio
            $resultadoPrecios->execute(array('1', '2', '3', 'A', $registroProductos["NumeroAPA"], $registroLista["idListaSAE"]));
            $registroPrecios = $resultadoPrecios->fetch(PDO::FETCH_ASSOC);
            if($registroPrecios["CVE_ART"]!=""){
                $resultadoInsertarProducto->execute(array(NULL, $registroPrecios["PRECIO"], $registroProductos["idProducto"], $registroPrecios["CVE_PRECIO"]));
                if($resultadoInsertarProducto->rowCount()==1){
                    $contador++;
                }
            }
        }
    }
    $resultadoProductos->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se insertaron un total de "  . $contador . " productos";
?>