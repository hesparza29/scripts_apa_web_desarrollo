<?php

    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $restriccion = "MOSTR";
    $estatus = "A";
    $contador = 0;
    //Consulta para actualizar la información de las facturas y remisiones
    $consultaActualizaInformacion = "UPDATE CARGAS SET DESCUENTO=?, VENDEDOR=? WHERE CLIENTE=?";
    $resultadoActualizaInformacion = $baseGodaddy->prepare($consultaActualizaInformacion);
    //Consulta para obtener el número de clientes de las facturas y remisiones
    $consultaClientes = "SELECT CLIE01.CLAVE, DESCUENTO, VEND01.NOMBRE 
                            FROM CLIE01 INNER JOIN VEND01 ON 
                            CLIE01.CVE_VEND=VEND01.CVE_VEND 
                            WHERE CLIE01.CLAVE!=? AND CLIE01.STATUS=?";
    $resultadoClientes = $baseSAE->prepare($consultaClientes);
    $resultadoClientes->execute(array($restriccion, $estatus));

    while($registroClientes = $resultadoClientes->fetch(PDO::FETCH_ASSOC)){
        $resultadoActualizaInformacion->execute(array($registroClientes["DESCUENTO"], 
                                                        $registroClientes["NOMBRE"], $registroClientes["CLAVE"]));
        $contador += $resultadoActualizaInformacion->rowCount();
    }
    $resultadoActualizaInformacion->closeCursor();
    $resultadoClientes->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizo la información de " . $contador . " facturas/remisiones";
?>