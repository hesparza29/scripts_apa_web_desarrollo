<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");


    $baseGodaddy = conexionBBDD_Godaddy();
    $contador = 0;
    $nombreArchivo = fopen("../archivos_de_carga/cambio sku en compras.csv", "r") or die("Problemas al abrir el archivo");

    //Consulta para obtener el nombre de la imagen
    $consultaObtenerNombreImagen = "SELECT Imagen FROM PRODUCTO WHERE NumeroAPA=?";
    $resultadoObtenerNombreImagen = $baseGodaddy->prepare($consultaObtenerNombreImagen);
    //Consulta para actualizar la imagen del sku
    $consultaActualizarImagen = "UPDATE PRODUCTO SET Imagen=? WHERE NumeroApa=?";
    $resultadoActualizarImagen = $baseGodaddy->prepare($consultaActualizarImagen);
    //Consulta para actualizar el sku dentro de la compra
    $consultaActualizarSku = "UPDATE COMPRA_PRODUCTO SET 
                                idProducto=(SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?) 
                                WHERE idProducto=(SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?)";
    $resultadoActualizarSku = $baseGodaddy->prepare($consultaActualizarSku);
    while (!feof($nombreArchivo)){  
        $linea = fgets($nombreArchivo);
        $linea = trim($linea);

        //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
        if(strlen($linea)>0){
          $linea = explode(",", $linea);
          echo $linea[0] . " " . $linea[1] . "<br />";
          //Obtiene nombre de la imagen
          $resultadoObtenerNombreImagen->execute(array($linea[0]));
          $registroObtenerNombreImagen = $resultadoObtenerNombreImagen->fetch(PDO::FETCH_ASSOC);
          //Actualiza el nombre la imagen
          $resultadoActualizarImagen->execute(array($registroObtenerNombreImagen["Imagen"], $linea[1]));
          //Actualiza el sku dentro de la compra
          $resultadoActualizarSku->execute(array($linea[1], $linea[0]));
          if($resultadoActualizarSku->rowCount()==1 || $resultadoActualizarImagen->rowCount()==1){
              $contador++;
          }
        }

    }
    $resultadoObtenerNombreImagen->closeCursor();
    $resultadoActualizarImagen->closeCursor();
    $resultadoActualizarSku->closeCursor();
    fclose($nombreArchivo);
    $baseGodaddy = null;

    echo "Se actualizaron un total de "  . $contador . " productos dentro de las compras";
?>