<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_AWS.php");

    $contador = 0;
    $imagen = "apa.jpg";
    $index = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseAWS = conexionBBDD_AWS();

    //Consulta para actualizar el nombre de la imagen en los productos
    $consultaActualizaImagen = "UPDATE PRODUCTO SET Imagen=? WHERE NumeroAPA=?";
    $resultadoActualizaImagen = $baseGodaddy->prepare($consultaActualizaImagen);

    //Consulta para obtener el nombre de la imagen del producto
    $consultaNombreImagen = "SELECT image 
                                FROM public.product_images 
                                INNER JOIN public.products ON product_images.product_id=products.id 
                                WHERE apa_id=? AND order_index=?";
    $resultadoNombreImagen = $baseAWS->prepare($consultaNombreImagen);

    //Consulta productos que aún no tengan un nombre de imagen asignado
    $consultaProductoSinImagen = "SELECT NumeroAPA FROM PRODUCTO";
    $resultadoProductosSinImagen = $baseGodaddy->prepare($consultaProductoSinImagen);
    $resultadoProductosSinImagen->execute(array());
    while($registroProductosSinImagen = $resultadoProductosSinImagen->fetch(PDO::FETCH_ASSOC)){
        $resultadoNombreImagen->execute(array($registroProductosSinImagen["NumeroAPA"], $index));
        $registroNombreImagen = $resultadoNombreImagen->fetch(PDO::FETCH_ASSOC);

        $resultadoActualizaImagen->execute(array($registroNombreImagen["image"], $registroProductosSinImagen["NumeroAPA"]));
        if ($resultadoActualizaImagen->rowCount()==1){
            $contador++;
        }
    }
    $resultadoActualizaImagen->closeCursor();
    $resultadoNombreImagen->closeCursor();
    $resultadoProductosSinImagen->closeCursor();

    

   

    $baseGodaddy = null;
    $baseAWS = null;

    echo "Se actualizo el nombre de imagen de " . $contador . " productos";


?>