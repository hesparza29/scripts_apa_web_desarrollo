<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $contador = 0;

    //Consulta para obtener NumeroAPA e Importancia de la vieja tabla
    $consultaImportancia = "SELECT Clavedeartículo, IMPORTANCIA FROM PRODUCTOS1";
    $resultadoImportancia =  $baseGodaddy->prepare($consultaImportancia);
    $resultadoImportancia->execute(array());

    //Consulta para actualizar la Importancia en la nueva tabla
    $consultaActualizaImportancia = "UPDATE PRODUCTO SET Importancia=? WHERE NumeroAPA=?";
    $resultadoActualizaImportancia = $baseGodaddy->prepare($consultaActualizaImportancia);

    while ($registroImportancia = $resultadoImportancia->fetch(PDO::FETCH_ASSOC)){
        //Ejecutando la consulta de actualizacion
        $resultadoActualizaImportancia->execute(array($registroImportancia["IMPORTANCIA"], 
                                                        $registroImportancia["Clavedeartículo"]));
        if($resultadoActualizaImportancia->rowCount()==1){
            $contador++;
        }
        $resultadoActualizaImportancia->closeCursor();
    }

    $resultadoImportancia->closeCursor();

    echo "Se actualizo la importancia de " . $contador . " productos";

    $baseGodaddy = null;


?>