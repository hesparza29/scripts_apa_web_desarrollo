<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Local.php");
    require_once("./funciones/conexionBBDD_Godaddy.php");

    $baseLocal = conexionBBDD_Godaddy();
    $contador = 0;
    
    //Consulta para actualizar el idPostal de la tabla Cliente
    $consultaActualizar = "UPDATE CLIENTE SET CP=? WHERE idCliente=?";
    $resultadoActualizar = $baseLocal->prepare($consultaActualizar);
    //Consulta para obtener los idPostal dado el codigo postal
    $consultaPostal = "SELECT idPostal, Codigo FROM POSTAL WHERE Codigo LIKE ? LIMIT 1";
    $resultadoPostal = $baseLocal->prepare($consultaPostal);
    //Consulta para obtener la información de la tabla Cliente
    $consultaCodigo = "SELECT idCliente, CP FROM CLIENTE WHERE NOT isnull(CP)";
    $resultadoCodigo = $baseLocal->prepare($consultaCodigo);
    $resultadoCodigo->execute(array());
    while($registroCodigo = $resultadoCodigo->fetch(PDO::FETCH_ASSOC)){
        //Seteamos el código postal al predeterminado '00000'
        if($registroCodigo["CP"]==0){
            $registroCodigo["CP"]='00000';
        }
        $resultadoPostal->execute(array('%' . $registroCodigo["CP"] . '%'));
        if ($resultadoPostal->rowCount()==1){
            
            $registroPostal = $resultadoPostal->fetch(PDO::FETCH_ASSOC);
            $resultadoActualizar->execute(array($registroPostal["idPostal"], $registroCodigo["idCliente"]));
            if($resultadoActualizar->rowCount()==1){
                $contador++;
            }
            else{
                echo $registroCodigo["CP"] . " NO<br />";
            }
        
        }
        else{
            echo $registroCodigo["CP"] . " NO<br />";
        }
        
        
    }
    $resultadoCodigo->closeCursor();
    $baseLocal = null;

    echo "Se actualizaron total de " . $contador . " codigos postales<br />";
?>