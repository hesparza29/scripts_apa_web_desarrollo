<?php
    ini_set('max_execution_time', 1500);
    require_once("./funciones/fecha_formato_BBDD.php");
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_Local.php");
    $fecha = array();
    $tipoDeCambio = array();
    $contador = 0;
    $nombreArchivo = fopen("..\\archivos_de_carga\\Historico Dolar.csv", "r") or die("Problemas al abrir el archivo");

    $base = conexionBBDD_Godaddy();

    //Consulta para insertar el historico del tipo de cambio
    $consulta = "INSERT INTO TIPO_DE_CAMBIO VALUES (?,?,?)";
    $resultado = $base->prepare($consulta);

    while(!feof($nombreArchivo)){
        $linea = fgets($nombreArchivo);
        $linea = trim($linea);

        //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
        if(strlen($linea)>0){
            $linea = explode(",", $linea);
            if($linea[0]!="Fecha"){
                $resultado->execute(array(NULL, fecha_formato_BBDD($linea[0]), $linea[1]));
                if($resultado->rowCount()==1){
                    $contador++;
                }
            }
        }
    }

    fclose($nombreArchivo);

    $base = null;

    echo "Se insertaron un total de " . $contador . " tipos de cambio.";
  
?>