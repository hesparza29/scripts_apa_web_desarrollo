<?php   
    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    require_once("./funciones/calcular_subtotal.php");
    require_once("./funciones/obtener_estatus.php");
    require_once("./funciones/fecha_actual.php");

    $tipo = "factura";
    $fecha = "";
    $lugar = "";
    $contador = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    //Consulta para verificar sí la factura ya se inserto
    $consultaVerificaFactura = "SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=? LIMIT 1";
    $resultadoVerificaFactura = $baseGodaddy->prepare($consultaVerificaFactura);
    //Consulta para insertar la información en la nueva tabla "REPORTE_DE_VENTAS"
    $consultaInsertarReporte = "INSERT INTO REPORTE_DE_VENTAS VALUES(?,?,?,?,?,?,
                                    (SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=? LIMIT 1),
                                    (SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=? LIMIT 1))";
    $resultadoInsertarReporte = $baseGodaddy->prepare($consultaInsertarReporte);
    //Consulta para insertar las nuevas facturas
    $consultaInsertaFacturas = "INSERT INTO CARGAS(idFacturaRemision, CLAVE, CLIENTE, NOMBRE, ESTATUS, FECHA, 
                                    DESCUENTO, IMPORTE, VENDEDOR, Entrada_Contado, Saldo)
                                    VALUES(?,?,?,?,?,?,?,?,?,?,?)";
    $resultadoInsertarFacturas = $baseGodaddy->prepare($consultaInsertaFacturas);
    //Consulta para obtener las partidas por factura
    $consultaPartidasFactura = "SELECT CVE_ART, CANT, DESC1, TOT_PARTIDA FROM PAR_FACTF01 WHERE CVE_DOC=?";
    $resultadoPartidasFactura = $baseSAE->prepare($consultaPartidasFactura);
    //Consulta para obtener las facturas posteriores a una fecha dada
    $consultaFacturas = "SELECT CVE_DOC, CVE_CLPV, FECHA_DOC, FACTF01.STATUS, DES_TOT_PORC, IMPORTE,
                            CLIE01.NOMBRE AS CLIENTE, VEND01.NOMBRE AS VENDEDOR, CLIE01.NOMBRE FROM FACTF01
                            INNER JOIN VEND01 ON FACTF01.CVE_VEND=VEND01.CVE_VEND
                            INNER JOIN CLIE01 ON FACTF01.CVE_CLPV=CLIE01.CLAVE
                            WHERE FECHA_DOC>? AND FECHA_DOC<?";
    $resultadoFacturas = $baseSAE->prepare($consultaFacturas);
    //Consulta para obtener la última fecha de factura
    $consultaFecha = "SELECT FECHA FROM CARGAS WHERE CLAVE LIKE ? ORDER BY FECHA DESC LIMIT 1";
    $resultadoFecha = $baseGodaddy->prepare($consultaFecha);
    $resultadoFecha->execute(array('%F%'));
    $registroFecha = $resultadoFecha->fetch(PDO::FETCH_ASSOC);
    $resultadoFecha->closeCursor();
    $fecha = $registroFecha["FECHA"];

    //Obteniendo las facturas
    $resultadoFacturas->execute(array($fecha, fecha_actual()));
    while($registroFacturas = $resultadoFacturas->fetch(PDO::FETCH_ASSOC)){
        //Verificando sí la factura existe para poder insertarla
        $resultadoVerificaFactura->execute(array($registroFacturas["CVE_DOC"]));
        if($resultadoVerificaFactura->rowCount()==0){
            //Insertar nuevas facturas
            $resultadoInsertarFacturas->execute(array(NULL, $registroFacturas["CVE_DOC"], $registroFacturas["CVE_CLPV"], 
                                                        $registroFacturas["CLIENTE"], obtenerEstatus($registroFacturas["STATUS"]), 
                                                        $registroFacturas["FECHA_DOC"], $registroFacturas["DES_TOT_PORC"], 
                                                        $registroFacturas["IMPORTE"], $registroFacturas["VENDEDOR"], 
                                                        0, $registroFacturas["IMPORTE"]));
            if($resultadoInsertarFacturas->rowCount()==1){
                $contador++;
                switch($registroFacturas["CVE_DOC"][1]){
                    case 'D':
                        $lugar = "Cedis Pozo Brasil";
                        break;

                    case 'T':
                        $lugar = "Cedis Tecámac";
                        break;

                    default:
                        $lugar = "Cedis Pozo Brasil";
                        break;
                }
                $resultadoPartidasFactura->execute(array($registroFacturas["CVE_DOC"]));
                while($registroPartidas = $resultadoPartidasFactura->fetch(PDO::FETCH_ASSOC)){
                    //Calcular el subtotal de cada partida
                    $subtotal = subtotal($registroPartidas["DESC1"], $registroPartidas["TOT_PARTIDA"]);
                    //Insertar la información dentro de la tabla "REPORTE_DE_VENTAS"
                    $resultadoInsertarReporte->execute(array(NULL, $subtotal, $registroPartidas["CANT"], 
                    obtenerEstatus($registroFacturas["STATUS"]), $tipo, $lugar, 
                    $registroFacturas["CVE_DOC"], $registroPartidas["CVE_ART"]));
                }
            }
        }
    }
    $resultadoVerificaFactura->closeCursor();
    $resultadoInsertarReporte->closeCursor();
    $resultadoInsertarFacturas->closeCursor();
    $resultadoPartidasFactura->closeCursor();
    $resultadoFacturas->closeCursor();
    
    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se insertaron un total de " . $contador . " facturas con sus respectivas partidas";
?>