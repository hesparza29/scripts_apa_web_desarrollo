<?php
    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $metodo = "Efectivo";
    $contador = 0;

    //Consulta para actualizar el total del cobro del día
    $consultaActualizarTotal = "UPDATE COBRO SET Total=? WHERE idCobro=?";
    $resultadoActualizarTotal = $baseGodaddy->prepare($consultaActualizarTotal);
    //Consulta para obtener el total del cobro del día
    $consultaTotalEfectivo = "SELECT 
                                (SELECT SUM(IMPORTE) FROM CARGAS WHERE ENTRADA=? AND METODO=?)-
                                (SELECT SUM(importeNotaDeCredito) AS Total FROM CARGAS WHERE ENTRADA=? AND METODO=?) AS Total";
    $resultadoTotalEfectivo = $baseGodaddy->prepare($consultaTotalEfectivo);
    //consulta para obtener el folio de la cobranza dentro de los cobros del día
    $consultaFolioCobranza = "SELECT A.idCobro, B.Folio FROM COBRO AS A 
                                INNER JOIN COBRANZA AS B ON A.idCobranza=B.idCobranza 
                                ORDER BY idCobro ASC";
    $resultadoFolioCobranza = $baseGodaddy->prepare($consultaFolioCobranza);
    $resultadoFolioCobranza->execute(array());

    while($registroFolioCobranza = $resultadoFolioCobranza->fetch(PDO::FETCH_ASSOC)){
        $resultadoTotalEfectivo->execute(array($registroFolioCobranza["Folio"], $metodo, $registroFolioCobranza["Folio"], $metodo));
        $registroTotalEfectivo = $resultadoTotalEfectivo->fetch(PDO::FETCH_ASSOC);
        $resultadoActualizarTotal->execute(array($registroTotalEfectivo["Total"], $registroFolioCobranza["idCobro"]));
        ($resultadoActualizarTotal->rowCount()==1) ? $contador++ : false;
    }
    $resultadoActualizarTotal->closeCursor();
    $resultadoTotalEfectivo->closeCursor();
    $resultadoFolioCobranza->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizo el total de " . $contador . " cobros del día.";

?>