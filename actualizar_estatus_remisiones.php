<?php

    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    
    $estatus = "C";
    $estatusFinal = "Cancelada";
    $fechaInicio = "";
    $fechaFin = "";
    $contador = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();

    //Consulta para actualizar el estatus de las remisiones
    $consultaActualizaRemisiones = "UPDATE CARGAS SET ESTATUS=? WHERE CLAVE=?";
    $resultadoActualizaRemisiones = $baseGodaddy->prepare($consultaActualizaRemisiones);
    //Consulta para actualizar el estatus del reporte de ventas
    $consultaActualizaReporte = "UPDATE REPORTE_DE_VENTAS SET Estatus=? 
                                    WHERE idFacturaRemision=(SELECT idFacturaRemision FROM CARGAS WHERE CLAVE=?)";
    $resultadoActualizaReporte = $baseGodaddy->prepare($consultaActualizaReporte);
    //Consulta para obtener la fecha de inicio
    $consultaFechaInicial = "SELECT DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AS FECHA FROM CARGAS LIMIT 1";
    $resultadoFechaInicial = $baseGodaddy->prepare($consultaFechaInicial);
    $resultadoFechaInicial->execute(array());
    $registroFechaInicial = $resultadoFechaInicial->fetch(PDO::FETCH_ASSOC);
    $fechaInicio = $registroFechaInicial["FECHA"];
    $resultadoFechaInicial->closeCursor();

    //Consulta para obtener la fecha final
    $consultaFechaFinal = "SELECT FECHA FROM CARGAS ORDER BY FECHA DESC LIMIT 1";
    $resultadoFechaFinal = $baseGodaddy->prepare($consultaFechaFinal);
    $resultadoFechaFinal->execute(array());
    $registroFechaFinal = $resultadoFechaFinal->fetch(PDO::FETCH_ASSOC);
    $fechaFin = $registroFechaFinal["FECHA"];
    $resultadoFechaFinal->closeCursor();

    //Consulta para obtener todas las remisiones canceladas en un rango de fecha
    $consultaRemisiones = "SELECT CVE_DOC FROM FACTR01 WHERE FECHA_DOC BETWEEN ? AND ? AND STATUS=?";
    $resultadoRemisiones = $baseSAE->prepare($consultaRemisiones);
    $resultadoRemisiones->execute(array($fechaInicio, $fechaFin, $estatus));
    while($registroRemisiones = $resultadoRemisiones->fetch(PDO::FETCH_ASSOC)){
        //Actualizando las remisiones
        $resultadoActualizaRemisiones->execute(array($estatusFinal, $registroRemisiones["CVE_DOC"]));
        //Actualizando las remisiones del reporte de ventas
        $resultadoActualizaReporte->execute(array($estatusFinal, $registroRemisiones["CVE_DOC"]));
        //Contabilizando cuantas remisiones se actualizaron
        if($resultadoActualizaRemisiones->rowCount()==1){
            $contador++;
        }
    }
    $resultadoActualizaRemisiones->closeCursor();
    $resultadoActualizaReporte->closeCursor();
    $resultadoRemisiones->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizaron un total de " . $contador . " remisiones.";
?>