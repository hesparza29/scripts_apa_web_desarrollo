<?php   
    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    require_once("./funciones/obtener_estatus_cliente.php");

    $estatus = "A";
    $remision = "Inactivo";
    $clave = "MOSTR";
    $contador = 0;

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();

    //Insertar los nuevos clientes SELECT IFNULL((SELECT idDescuento FROM DESCUENTO WHERE Porcentaje=90), 1);
    $consultaInsertaCliente = "INSERT INTO CLIENTE VALUES(?,?,
                                    (SELECT idDescuento FROM DESCUENTO WHERE Porcentaje=?),
                                    ?,?,?,?,?,?,?,
                                    (SELECT idVendedor FROM VENDEDOR WHERE idVendedor=?))";
    $resultadoInsertaCliente = $baseGodaddy->prepare($consultaInsertaCliente);
    //Consulta para verificar que el cliente ya se encuentra guardado
    $consultaClienteGuardado = "SELECT Nombre FROM CLIENTE WHERE idCliente=?";
    $resultadoClienteGuardado = $baseGodaddy->prepare($consultaClienteGuardado);
    //Consulta para obtener los clientes activos
    $consultaClientes = "SELECT CLAVE, NOMBRE, DESCUENTO, RFC, CALLE, COLONIA,
                            CODIGO, TELEFONO, STATUS, CVE_VEND FROM CLIE01 
                            WHERE STATUS=? AND CLAVE!=? 
                            ORDER BY CLAVE ASC";
    $resultadoClientes = $baseSAE->prepare($consultaClientes);
    $resultadoClientes->execute(array($estatus, $clave));
    while($registroClientes = $resultadoClientes->fetch(PDO::FETCH_ASSOC)){
        $resultadoClienteGuardado->execute(array($registroClientes["CLAVE"]));
        if(!$resultadoClienteGuardado->rowCount()==1){
            $resultadoInsertaCliente->execute(array($registroClientes["CLAVE"], $registroClientes["NOMBRE"], (empty($registroClientes["DESCUENTO"]) ? 1 : $registroClientes["DESCUENTO"]), 
                                                    $registroClientes["RFC"], $registroClientes["CALLE"], $registroClientes["COLONIA"], 
                                                    $registroClientes["CODIGO"], $registroClientes["TELEFONO"], obtenerEstatusCliente($registroClientes["STATUS"]), 
                                                    $remision, (empty($registroClientes["CVE_VEND"]) ? 1 : $registroClientes["CVE_VEND"])));
            if($resultadoInsertaCliente->rowCount()==1){
                $contador++;
            }
        }
        
    }
    $resultadoInsertaCliente->closeCursor();
    $resultadoClienteGuardado->closeCursor();
    $resultadoClientes->closeCursor();
    
    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se insertaron un total de " . $contador . " clientes";
?>