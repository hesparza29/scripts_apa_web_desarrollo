<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");


    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $contador = 0;
    //Consulta para insertar productos
    $consultaInsertaProducto = "INSERT INTO PRODUCTO (idProducto, NumeroAPA, Descripcion, Importancia, idLinea, idSublinea, idFamilia) 
                                    VALUES(?,?,?,?,?,
                                            (SELECT idSublinea FROM SUBLINEA WHERE Descripcion LIKE ? LIMIT 1),
                                            (SELECT idFamilia FROM FAMILIA WHERE Descripcion LIKE ? LIMIT 1))";
    $resultadoInsertaProducto = $baseGodaddy->prepare($consultaInsertaProducto);
    //Consulta para verificar que no existe el producto en la base de datos
    $consultaProducto = "SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?";
    $resultadoProducto = $baseGodaddy->prepare($consultaProducto);
    //Consulta para obtener los productos activos del SAE
    $consultaProductos = "SELECT CVE_ART, DESCR, LIN_PROD, 
                            CAMPLIB8, CAMPLIB1
                            FROM INVE01 INNER JOIN
                            INVE_CLIB01 ON
                            INVE01.cve_art=INVE_CLIB01.cve_prod
                            WHERE (LIN_PROD=? OR LIN_PROD=? OR LIN_PROD=?)
                            AND STATUS=?";
    $resultadoProductos = $baseSAE->prepare($consultaProductos);
    $resultadoProductos->execute(array('1', '2', '3', 'A'));

    while ($registroProductos = $resultadoProductos->fetch(PDO::FETCH_ASSOC)){
        
        $resultadoProducto->execute(array($registroProductos["CVE_ART"]));
        if($resultadoProducto->rowCount()==0){
            
            $resultadoInsertaProducto->execute(array(NULL, $registroProductos["CVE_ART"], 
                                                        $registroProductos["DESCR"], 'C', $registroProductos["LIN_PROD"],
                                                        "%" . $registroProductos["CAMPLIB8"] . "%", 
                                                        "%" . $registroProductos["CAMPLIB1"] . "%"));
            if($resultadoInsertaProducto->rowCount()==1){
                $contador++;
            }
            $resultadoInsertaProducto->closeCursor();
        }

        $resultadoProducto->closeCursor();

        
    }

    $resultadoProductos->closeCursor();

    echo "Se insertaron un total de " . $contador . " productos<br />";
    $baseGodaddy = null;
    $baseSAE = null;

?>