<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $contador = 0;

    // //Actualizar información de los clientes
    // $actualizarInformacionClientes = "UPDATE CLIENTE SET idVendedor=? WHERE idCliente=?";
    // $resultadoActualizarInformacionClientes = $baseGodaddy->prepare($actualizarInformacionClientes);
    // //Obtener información actualizada de los clientes
    // $consultaClientes = "SELECT CLAVE, CVE_VEND FROM CLIE01 WHERE STATUS=?";
    // $resultadoClientes = $baseSAE->prepare($consultaClientes);
    // $resultadoClientes->execute(array('A'));
    // while($registroClientes = $resultadoClientes->fetch(PDO::FETCH_ASSOC)){
    //     $resultadoActualizarInformacionClientes->execute(array($registroClientes["CVE_VEND"], $registroClientes["CLAVE"]));
    //     ($resultadoActualizarInformacionClientes->rowCount()==1) ? $contador++: false;
    // }
    // $resultadoClientes->closeCursor();
    
    //Actualizar vendedor de las notas
    $consultaActualizarVendedor = "UPDATE NOTA SET idVendedor=(SELECT idVendedor FROM CLIENTE WHERE idCliente=?) WHERE idNota=?";
    $resultadoActualizarVendedor = $baseGodaddy->prepare($consultaActualizarVendedor);
    //consulta notas
    $consultaNotas = "SELECT idNota, idCliente FROM NOTA ORDER BY idNota ASC";
    $resultadoNotas = $baseGodaddy->prepare($consultaNotas);
    $resultadoNotas->execute(array());
    while($registroNotas = $resultadoNotas->fetch(PDO::FETCH_ASSOC)){
        $resultadoActualizarVendedor->execute(array($registroNotas["idCliente"], $registroNotas["idNota"]));
        ($resultadoActualizarVendedor->rowCount()==1) ? $contador++ : false;
    }
    $resultadoActualizarVendedor->closeCursor();
    $resultadoNotas->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizo la información de " . $contador . " clientes";


?>