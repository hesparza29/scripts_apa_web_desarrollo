<?php

    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");
    require_once("./funciones/calcular_subtotal.php");


    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $estatus = "Emitida";
    $fechaInicio = "2021-06-10";
    $fechaFin = "2021-06-10";
    $tipo = "";
    $lugar = "";
    $subtotal = 0;
    $contador = 0;

    //Consulta para insertar la información en la nueva tabla "REPORTE_DE_VENTAS"
    $consultaInsertarReporte = "INSERT INTO REPORTE_DE_VENTAS VALUES(?,?,?,?,?,?,?,
                                    (SELECT idProducto FROM PRODUCTO WHERE NumeroAPA=?))";
    $resultadoInsertarReporte = $baseGodaddy->prepare($consultaInsertarReporte);
    //Consulta para obtener las partidas por remision
    $consultaPartidasRemision = "SELECT CVE_ART, CANT, DESC1, TOT_PARTIDA FROM PAR_FACTR01 WHERE CVE_DOC=?";
    $resultadoPartidasRemision = $baseSAE->prepare($consultaPartidasRemision);
    //Consulta para obtener las partidas por factura
    $consultaPartidasFactura = "SELECT CVE_ART, CANT, DESC1, TOT_PARTIDA FROM PAR_FACTF01 WHERE CVE_DOC=?";
    $resultadoPartidasFactura = $baseSAE->prepare($consultaPartidasFactura);
    //Consulta para obtener todas las facturas y remisiones
    $consultaFacturaRemision = "SELECT idFacturaRemision, CLAVE, ESTATUS FROM CARGAS WHERE ESTATUS=? AND 
                                FECHA BETWEEN ? AND ?";
    $resultadoFacturaRemision = $baseGodaddy->prepare($consultaFacturaRemision);
    $resultadoFacturaRemision->execute(array($estatus, $fechaInicio, $fechaFin));
    while($registroFacturaRemision = $resultadoFacturaRemision->fetch(PDO::FETCH_ASSOC)){
        //Obtener el lugar de la venta y saber si son facturas o remisiones
        switch($registroFacturaRemision["CLAVE"][0]){
            case 'F':
                $tipo = "factura";
                switch($registroFacturaRemision["CLAVE"][1]){
                    case 'D':
                        $lugar = "Cedis Pozo Brasil";
                        break;
                    
                    case 'T':
                        $lugar = "Cedis Tecámac";
                        break;
                }
                break;
            
            case 'R':
                $tipo = "remision";
                switch($registroFacturaRemision["CLAVE"][1]){
                    case 'R':
                        $lugar = "Cedis Pozo Brasil";
                        break;
                    
                    case 'T':
                        $lugar = "Cedis Tecámac";
                        break;
                }
                break;
        }
        //Verificar que tipo de documento es para ejecutar la consulta correspondiente
        if($tipo=="factura"){
            $resultadoPartidasFactura->execute(array($registroFacturaRemision["CLAVE"]));
            while($registroPartidas = $resultadoPartidasFactura->fetch(PDO::FETCH_ASSOC)){
                //Calcular el subtotal de cada partida
                $subtotal = subtotal($registroPartidas["DESC1"], $registroPartidas["TOT_PARTIDA"]);
                //Insertar la información dentro de la tabla "REPORTE_DE_VENTAS"
                $resultadoInsertarReporte->execute(array(NULL, $subtotal, $registroPartidas["CANT"], 
                                                    $registroFacturaRemision["ESTATUS"], $tipo, $lugar, 
                                                    $registroFacturaRemision["idFacturaRemision"], $registroPartidas["CVE_ART"]));
            }
        }
        else if($tipo=="remision"){
            $resultadoPartidasRemision->execute(array($registroFacturaRemision["CLAVE"]));
            while($registroPartidas = $resultadoPartidasRemision->fetch(PDO::FETCH_ASSOC)){
                //Calcular el subtotal de cada partida
                $subtotal = subtotal($registroPartidas["DESC1"], $registroPartidas["TOT_PARTIDA"]);
                //Insertar la información dentro de la tabla "REPORTE_DE_VENTAS"
                $resultadoInsertarReporte->execute(array(NULL, $subtotal, $registroPartidas["CANT"], 
                                                    $registroFacturaRemision["ESTATUS"], $tipo, $lugar, 
                                                    $registroFacturaRemision["idFacturaRemision"], $registroPartidas["CVE_ART"]));
            }
        }
        $contador++;
    }
    $resultadoInsertarReporte->closeCursor();
    $resultadoPartidasRemision->closeCursor();
    $resultadoPartidasFactura->closeCursor();
    $resultadoFacturaRemision->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se insertaron un total de " . $contador . " facturas/remisiones con sus respectivas partidas";
    
?>