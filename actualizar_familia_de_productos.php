<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $contador = 0;
    //Consulta para actualizar el la familia de cada producto
    $consultaActualizaFamilia = "UPDATE PRODUCTO SET 
                                    idFamilia=(SELECT idFamilia FROM FAMILIA WHERE Descripcion=?) 
                                    WHERE NumeroAPA=?";
    $resultadoActualizaFamilia = $baseGodaddy->prepare($consultaActualizaFamilia);
    //Consulta para obtener la familia de un producto
    $consultaFamilia = "SELECT CVE_PROD, CAMPLIB1 FROM INVE_CLIB01";
    $resultadoFamilia = $baseSAE->prepare($consultaFamilia);
    $resultadoFamilia->execute(array());
    while($registroFamilia = $resultadoFamilia->fetch(PDO::FETCH_ASSOC)){
        
        $resultadoActualizaFamilia->execute(array($registroFamilia["CAMPLIB1"], 
                                                    $registroFamilia["CVE_PROD"]));
        if($resultadoActualizaFamilia->rowCount()==1){
            $contador++;    
        }
    }
    $resultadoActualizaFamilia->closeCursor();
    $resultadoFamilia->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizo la familia a " . $contador . " productos.";
?>