<?php
    ini_set('max_execution_time', 1500);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_Local.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $contador = 0;
    $fechaInicio = "2021-01-01";
    $fechaFin = "2021-03-31";
    $estatus = "A";
    //Consulta para obtener el importe de una factura
    $consultaImporte = "SELECT IMPORTE FROM CARGAS WHERE CLAVE=?";
    $resultadoImporte = $baseGodaddy->prepare($consultaImporte);
    //Consulta para obtener los abonos
    $consultaAbono = "SELECT NO_FACTURA, SUM(cuen_det01.IMPORTE) AS SALDO 
                        FROM cuen_det01
                        INNER JOIN factf01 ON
                        cuen_det01.no_factura=factf01.cve_doc
                        WHERE TIPO_MOV=? AND FECHA_DOC BETWEEN
                        ? AND ?
                        GROUP BY NO_FACTURA HAVING count(*)>0";
    $resultadoAbono = $baseSAE->prepare($consultaAbono);
    //Consulta actualizar el saldo de las facturas
    $consultaActualizarSaldo = "UPDATE CARGAS SET Saldo=? WHERE CLAVE=?";
    $resultadoActualizarSaldo = $baseGodaddy->prepare($consultaActualizarSaldo);
    $resultadoAbono->execute(array($estatus, $fechaInicio, $fechaFin));
    while($registroAbono = $resultadoAbono->fetch(PDO::FETCH_ASSOC)){
        $resultadoImporte->execute(array($registroAbono["NO_FACTURA"]));
        $registroImporte = $resultadoImporte->fetch(PDO::FETCH_ASSOC);
        $resultadoActualizarSaldo->execute(array(($registroImporte["IMPORTE"]-$registroAbono["SALDO"]), $registroAbono["NO_FACTURA"]));
        if($resultadoActualizarSaldo->rowCount()==1){
            $contador++;
        }
    }
    $resultadoActualizarSaldo->closeCursor();
    $resultadoAbono->closeCursor();

    $baseGodaddy = null;
    $baseSAE = null;

    echo "Se actualizo el saldo de " . $contador . " facturas.";
?>