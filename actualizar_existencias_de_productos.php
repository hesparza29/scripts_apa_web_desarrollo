<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_Godaddy.php");
    require_once("./funciones/conexionBBDD_SAE.php");

    $baseGodaddy = conexionBBDD_Godaddy();
    $baseSAE = conexionBBDD_SAE();
    $contador = 0;

    //Consulta para actualizar existencias
    $consultaActualizarExistencias = "UPDATE PRODUCTO SET Existencia=? WHERE NumeroAPA=?";
    $resultadoActualizarExistencias = $baseGodaddy->prepare($consultaActualizarExistencias);
    //Consulta existencia de productos
    $consultaExistencias = "SELECT CVE_ART, EXIST 
                            FROM INVE01 INNER JOIN
                            INVE_CLIB01 ON
                            INVE01.cve_art=INVE_CLIB01.cve_prod
                            WHERE (LIN_PROD=? OR LIN_PROD=? OR LIN_PROD=?)
                            AND STATUS=?";
    $resultadoExistencias = $baseSAE->prepare($consultaExistencias);
    $resultadoExistencias->execute(array('1', '2', '3', 'A'));
    while($registroExistencias = $resultadoExistencias->fetch(PDO::FETCH_ASSOC)){
        $resultadoActualizarExistencias->execute(array($registroExistencias["EXIST"], $registroExistencias["CVE_ART"]));
        if($resultadoActualizarExistencias->rowCount()==1){
            $contador++;
        }
    }
    $resultadoActualizarExistencias->closeCursor();
    $resultadoExistencias->closeCursor();

    echo "Se actualizo la existencia de " . $contador . " productos.";

    $baseGodaddy = null;
    $baseSAE = null;
?>